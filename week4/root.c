#include <stdio.h>
#include <stdlib.h>
#include <math.h>

FILE *outp;

float arb_func(float x){
    return x*log10(x) - 1.2;
}

void rf(){
    float *x;
    float x1;
    float x2;
    float fx0;
    float fx1;
    int *itr;

    *x= (x1*fx0)- (x2*fx1)/(fx1-fx0);
    ++*itr;
    printf("number of iteration: %d approx root: %f\n",*itr,*x);
    fprintf(outp,"iteration: %d, approx root: %f\n",*itr,*x);

}
int main()
{
    int itr;
    int maxitr;
    float x0;
    float x1;
    float x_curr;
    float x_next;
    float error;
    float fx0;

    outp=fopen("rf.txt","w");

    printf("please enter x0: \n");
    scanf("%lf",&x0);

    printf("please enter x1: \n");
    scanf("%lf",&x1);

    printf("please enter x_curr: \n");
    scanf("%lf",&x_curr);

    printf("please enter error: \n");
    scanf("%lf",&error);

    rf(&x_curr,&x0,&x1,arb_func(x0),arb_func(x1),&itr);

    do{

        if(fx0*x_curr<0){
            x1=x_curr;
        }else
            x0=x_curr;

         rf(&x_next,&x0,&x1,arb_func(x0),arb_func(x1),&itr);

        if (fabs(x_next-x_curr) < error){
            printf("You reached the root");
            fprintf(outp,"You reached the root\n");
            return 0;
        }else
            continue;
            x_curr=x_next;


    }while(itr<maxitr);



    printf("You cannot converge");
    fprintf(outp,"You cannot converge");

    fclose(outp);

    return 1;
}
