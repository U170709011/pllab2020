#include <stdio.h>
#include <stdlib.h>

void menu(void);
void convertTextFile(FILE *fPtr);
void addRecord(FILE *fPtr);
void deleteRecord(FILE *fPtr);
void updateRecord(FILE *fPtr);
void showRecords(FILE *fPtr);


typedef struct{
    int id;
    char name[20];
    char department[20];
    int birth;
    int salary;
}employeeData;

int main()
{
    menu();
    return 0;
}

void menu(void){

    FILE *fPtr;


    int choice;

    printf("1 - add new record\n2 - update record\n3 - delete record\n4 - print all records\n5 - save as txt file\n6 - end program\n");

    printf("ENTER A CHOICE => ");

    scanf("%d",&choice);

    switch (choice){
        case '1':
            addRecord(fPtr);

            break;
        case '2':
            updateRecord(fPtr);

            break;
        case '3':
            deleteRecord(fPtr);

            break;
        case '4':
            showRecords(fPtr);

            break;
        case '5':
            convertTextFile(fPtr);

            break;
        case '6':
            fclose(fPtr);
            break;

    }

}

void addRecord(FILE *fPtr){

    fPtr = fopen("employee.bin","rb+");


    employeeData data;

    int add_id;
    printf("Enter id to create new employee record: ");
    scanf("%d",&add_id);

      if(fPtr==NULL){
        printf("There is no such a file.");
    }else{
    fseek(fPtr,(add_id-1)*sizeof(employeeData),SEEK_SET);
    fread(&data,sizeof(employeeData),1,fPtr);
    if(data.id==add_id){
        printf("You can not add an employee with same ID with others");
    }
    else{
        printf("Employee Name:");
        printf("\nDepartment:");
        scanf("%s",data.department);
        printf("\nBirth Year:");
        scanf("%d",&data.birth);
        printf("\nSalary:");
        scanf("%d",&data.salary);
        printf("Employee with ID #%d is recorded.",add_id);
    }

    }
    fclose(fPtr);
}

void updateRecord(FILE *fptr){
    FILE *fPtr=fopen("employee.bin","rb+");


    employeeData data;
    float increase;
    int ID;
    printf("Enter the ID to update employee record:");
    scanf("%d",&ID);
    if(fptr==NULL){
        printf("There is no such a file.");
    }
    else{
    fseek(fptr,(ID-1)*sizeof(employeeData),SEEK_SET);
    fread(&data,sizeof(employeeData),1,fptr);
        if(data.id!=ID){
            printf("You can not update this record because there is no such a record.");
                       }
        else{
            printf("ID\tEmployee Name\tDepartment\tBirth Year\tSalary\n%d\t%s\t%s\t%d\t%d\n ",data.id,data.name,data.department,data.birth,data.salary);
            printf("\nEnter the percentage of increase for salary:");
            scanf("%f",&increase);
            data.salary+=(data.salary*(increase/100));
            printf("ID\tEmployee Name\tDepartment\tBirth Year\tSalary\n%d\t%s\t%s\t%d\t%d\n ",data.id,data.name,data.department,data.birth,data.salary);

            }

    }

    fclose(fPtr);
}

void deleteRecord(FILE *fPtr){

    fPtr = fopen("employee.bin","rb+");

    employeeData data;
    int ID;
    printf("Enter the ID to delete employee record:");
    scanf("%d",&ID);

    fclose(fPtr);
}




void showRecords(FILE *fPtr){

    fPtr = fopen("employee.bin","rb+");


    employeeData datas[100];
    int i;
    if(fPtr==NULL){
        printf("There is no such a file.");
    }
    else{
        fread(datas,sizeof(employeeData),100,fPtr);
        for(i=0;i<100;i++){
            if(datas[i].id!=0){
                printf("ID\tEmployee Name\tDepartment\tBirth Year\tSalary\n%d\t%s\t%s\t%d\t%d\n ",datas[i].id,datas[i].name,datas[i].department,datas[i].birth,datas[i].salary);
                printf("\n");
            }
        }

    }
    fclose(fPtr);
}


void convertTextFile(FILE *fPtr){
    fPtr = fopen("employee.bin","rb+");

    unsigned char buffer[50];
    fPtr = fopen("employee.bin","rb");
    fread(buffer,sizeof(buffer),1,fPtr);

    for(int i = 0; i<50; i++){
        printf("%u ", buffer[i]);
    }


    fclose(fPtr);
}


