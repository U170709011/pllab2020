
#include <stdio.h>
#include <stdlib.h>



int nrDigits(int num)
{
    static int count;

    if (num>0)
    {
        count=count+1;
        nrDigits(num/10);
    }
    else
        return count;

}

int main(void)
{
    int num;
    int count=0;

    printf("enter a number");
    scanf("%d",&num);

    count = nrDigits(num);
    printf("total digit of number %d is: %d\n",num,count);

    return 0;
}
